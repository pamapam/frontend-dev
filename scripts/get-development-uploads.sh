#!/bin/bash

set -x

mkdir tmp

ssh ftp_pamapam@web.pangea.org " cd /home/setem/webapps/pamapam_wp/wp-content && zip -r -0 /tmp/pamapam-uploads.zip uploads"
scp ftp_pamapam@web.pangea.org:/tmp/pamapam-uploads.zip ./uploads.zip
ssh ftp_pamapam@web.pangea.org " rm /tmp/pamapam-uploads.zip"

mv uploads.zip ../resources
