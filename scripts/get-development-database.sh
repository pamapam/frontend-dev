#!/bin/bash

set -x

read -sp 'Wordpress db password: ' PASSWORD

mkdir tmp

ssh ftp_pamapam@web.pangea.org " mysql -h mysql.pangea.lan pamapam_wp -u pamapam_wp -p$PASSWORD -N -e 'show tables where (Tables_in_pamapam_wp like \"wpstg1\\_%\" and Tables_in_pamapam_wp not like \"wpstg1\\_wpstg%\")' | xargs mysqldump -h mysql.pangea.lan -u pamapam_wp -pcefabd0fec pamapam_wp > /tmp/pamapam_wp_stg.sql"
scp ftp_pamapam@web.pangea.org:/tmp/pamapam_wp_stg.sql tmp/
ssh ftp_pamapam@web.pangea.org " rm /tmp/pamapam_wp_stg.sql"

mkdir -p tmp/nginx
git clone https://gitlab.com/pamapam/frontend.git tmp/web

cp resources/default.conf tmp/nginx
cp resources/wp-config.php tmp/web

docker run --name pamapam-stg-web-db -e MYSQL_ROOT_PASSWORD=pamapam -d mariadb:10 --log-bin=ON
sleep 30
docker exec pamapam-stg-web-db sh -c 'exec mysql -uroot -p"$MYSQL_ROOT_PASSWORD" -e "create database pamapam_wp default character set utf8 collate utf8_general_ci;"'
docker exec -i pamapam-stg-web-db sh -c 'exec mysql -u root -p"$MYSQL_ROOT_PASSWORD" --default-character-set=utf8 pamapam_wp ' < tmp/pamapam_wp_stg.sql

docker network create pamapam-stg-net
docker network connect --alias mysql --alias mariadb pamapam-stg-net pamapam-stg-web-db

# ... Disable plugins.

docker run --rm --network=pamapam-stg-net -v `pwd`/tmp/web:/var/www/html nickbreen/wp-cli:v3.0.0 wp --allow-root --path=/var/www/html --skip-plugins=wp-staging,wp-staging-pro plugin deactivate jamgo-login wp-staging wp-staging-pro xcloner-backup-and-restore versionpress

# ... Rename domains & prefixes.

docker run --rm --network=pamapam-stg-net -v `pwd`/tmp/web:/var/www/html nickbreen/wp-cli:v3.0.0 wp --allow-root --all-tables-with-prefix --path=/var/www/html search-replace "wpstg1_" "wp_"
docker run --rm --network=pamapam-stg-net -v `pwd`/tmp/web:/var/www/html nickbreen/wp-cli:v3.0.0 wp --allow-root --all-tables-with-prefix --path=/var/www/html search-replace "https://pamapam.org/comunitatdigital" "http://pamapam.local"
docker run --rm --network=pamapam-stg-net -v `pwd`/tmp/web:/var/www/html nickbreen/wp-cli:v3.0.0 wp --allow-root --all-tables-with-prefix --path=/var/www/html search-replace "pamapam.org/comunitatdigital" "pamapam.local"

# ... Remove user tables.

docker exec pamapam-stg-web-db sh -c 'exec mysql -u root -p"$MYSQL_ROOT_PASSWORD" -e "drop table pamapam_wp.wpstg1_users;"'
docker exec pamapam-stg-web-db sh -c 'exec mysql -u root -p"$MYSQL_ROOT_PASSWORD" -e "drop table pamapam_wp.wpstg1_usermeta;"'

docker exec -i pamapam-stg-web-db sh -c 'exec mysqldump -u root -p"$MYSQL_ROOT_PASSWORD" pamapam_wp' > tmp/pamapam_wp_wpstg1.sql
sed 's/wpstg1_/wp_/g' tmp/pamapam_wp_wpstg1.sql > pamapam_wp_development.sql

docker stop pamapam-stg-web-db
docker rm pamapam-stg-web-db
docker network rm pamapam-stg-net
sudo rm -rf tmp

mv pamapam_wp_development.sql ../resources
