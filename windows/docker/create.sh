#!/bin/bash
RESOURCES=../../resources

mkdir -p ~/pamapam/docker-data/nginx
git clone https://gitlab.com/pamapam/frontend.git ~/pamapam/docker-data/web

cp $RESOURCES/docker/default.conf ~/pamapam/docker-data/nginx
cp $RESOURCES/docker/wp-config.php ~/pamapam/docker-data/web
unzip $RESOURCES/uploads.zip -d ~/pamapam/docker-data/web/wp-content

docker run --name pamapam-web-db --platform linux -e MYSQL_ROOT_PASSWORD=pamapam -d mariadb:10 --log-bin=ON
sleep 30
docker exec -i pamapam-web-db sh -c 'exec mysql -u root -p"$MYSQL_ROOT_PASSWORD" --default-character-set=utf8 ' < $RESOURCES/pamapam_wp_development.sql

docker run --name pamapam-web --platform linux --link pamapam-web-db:mysql -v $USERPROFILE\\pamapam\\docker-data\\web:/var/www/html -d php:7.1-fpm-alpine
docker exec pamapam-web sh -c 'docker-php-ext-install mysqli'

docker run --name pamapam --platform linux --link pamapam-web:wordpress -v $USERPROFILE\\pamapam\\docker-data\\nginx:/etc/nginx/conf.d -v $USERPROFILE\\pamapam\\docker-data\\web:/var/www/html -p 80:80 -d nginx:alpine