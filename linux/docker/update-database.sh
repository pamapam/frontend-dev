#!/bin/bash
RESOURCES=../../resources

docker exec -i pamapam-web-db sh -c 'exec mysql -u root -p"$MYSQL_ROOT_PASSWORD" --default-character-set=utf8 pamapam_wp ' < $RESOURCES/pamapam_wp_development.sql
