#!/bin/bash
RESOURCES=../../resources

mkdir -p ~/pamapam/docker-data/nginx
git clone https://gitlab.com/pamapam/frontend.git ~/pamapam/docker-data/web

cp $RESOURCES/docker/default.conf ~/pamapam/docker-data/nginx
cp $RESOURCES/docker/wp-config.php ~/pamapam/docker-data/web
unzip $RESOURCES/uploads.zip -d ~/pamapam/docker-data/web/wp-content

docker run --name pamapam-web-db -e MYSQL_ROOT_PASSWORD=pamapam -d mariadb:10 --log-bin=ON
sleep 30

docker exec pamapam-web-db sh -c 'exec mysql -uroot -p"$MYSQL_ROOT_PASSWORD" -e "create database pamapam_wp default character set utf8 collate utf8_general_ci;"'
docker exec -i pamapam-web-db sh -c 'exec mysql -u root -p"$MYSQL_ROOT_PASSWORD" --default-character-set=utf8 pamapam_wp ' < $RESOURCES/pamapam_wp_development.sql

DOCKER_HOST_IP=$(docker run --rm nginx:alpine sh -c "/sbin/ip route" | awk '/default/ { print $3 }')

docker run --name pamapam-web --add-host pamapam.local:$DOCKER_HOST_IP --link pamapam-web-db:mysql -v ~/pamapam/docker-data/web:/var/www/html -d php:7.1-fpm-alpine
docker exec pamapam-web sh -c 'docker-php-ext-install mysqli'
docker restart pamapam-web

docker run --name pamapam --link pamapam-web:wordpress -v ~/pamapam/docker-data/nginx:/etc/nginx/conf.d -v ~/pamapam/docker-data/web:/var/www/html -p 80:80 -d nginx:alpine
sudo -- sh -c "echo '127.0.0.1 pamapam.local' >> /etc/hosts"
